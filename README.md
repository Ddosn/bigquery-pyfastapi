# BigQuery Python FastAPI Example

### Goals
- simple python app with bigquery as a data store
- table should have at least one column of RECORD type
- eventual data consistency is okay
- stretch goal: use fast api instead of flask


### Getting started
- Set environment variable GOOGLE_APPLICATION_CREDENTIALS
- Clone git repo
- cd to project folder
- create virtual environment
- pip install requirements.txt


### BigQuery Schema
```json
[{
		"name": "id",
		"type": "INTEGER",
		"mode": "REQUIRED"
	},
	{
		"name": "app_name",
		"type": "STRING",
		"mode": "REQUIRED"
	},
	{
		"name": "userid",
		"type": "INTEGER",
		"mode": "NULLABLE"
	},
	{
		"name": "more_info",
		"type": "STRING",
		"mode": "REQUIRED"
	},
	{
		"name": "timestamp",
		"type": "DATETIME",
		"mode": "REQUIRED"

	},
	{
		"name": "action",
		"type": "RECORD",
		"mode": "NULLABLE",
		"fields": [{
				"name": "type",
				"type": "STRING"
			},
			{
				"name": "sub_type",
				"type": "STRING"
			}
		]
	}
]
```


### Start server
```shell script
cd bigquery-fastapi/src
uvicorn app.main:app --reload
```


### Sample POST /logs
```json
[
  {
    "app_name": "Twitter",
    "userid": 873738,
    "more_info": "added twitter app to the account",
    "timestamp": "2020-04-01 11:55:23",
    "action": {
      "type": "app purchase",
      "sub_type": "promotion"
    }
  }
]
```