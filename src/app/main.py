from fastapi import FastAPI
from app.api import audit

app = FastAPI()

app.include_router(audit.router)